import { NextPage, NextPageContext } from 'next'
import dynamic from 'next/dynamic'
import * as React from 'react'
import { getLabels } from '~/db/labels'
import { LabelCategory } from '~/types'
import { LabelsForm } from '~/ui/annotation/LabelsForm'
import FullPageLayout from '~/ui/FullPageLayout'

const VideoFrame = dynamic(() => import('~/ui/video/VideoFrame'), {
  ssr: false
})

const SAMPLE_URL = '/static/road_sample.jpg'

type Props = {
  labels: LabelCategory[]
}

export async function getServerSideProps({
  query
}: NextPageContext): Promise<{ props: Props }> {
  const labels = await getLabels()
  return { props: { labels } }
}

export const HomePage: NextPage<Props> = ({ labels }: Props) => {
  function onSubmit(labels) {
    console.log('Labels', labels)
    alert(`Labels: ${JSON.stringify(labels)}`) // TODO: send to API server
  }

  return (
    <FullPageLayout title="Video Frame Tagging MVP">
      <div>
        <h1>Video Frame Tagging MVP (with zoom &amp; panning)</h1>
        <LabelsForm labels={labels} onSubmit={onSubmit} />
        <VideoFrame src={SAMPLE_URL} />
        <p className="text-info">
          Tip: Click on photo to change zoom level. Move mouse over image to
          change zoomed area.
        </p>
      </div>
    </FullPageLayout>
  )
}

export default HomePage
