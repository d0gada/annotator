import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { LabelCategory } from '~/types'

type Data = {
  [category: string]: string
}

type Props = {
  labels: LabelCategory[]
  onSubmit: (data: Data) => void
}

export function LabelsForm({ labels, onSubmit }: Props): React.ReactElement {
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<Data>({
    defaultValues: {}
  })
  return (
    <div>
      <div>
        Please select valid tags bellow and press <em>Submit</em> button (
        <strong>bold fields</strong> are required).
      </div>
      <form className="form-inline my-2" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          {labels.map((l) => (
            <select
              key={l.category}
              {...register(l.category, {
                required: l.required
              })}
              className={clsx(
                errors[l.category] && 'is-invalid',
                l.required && 'font-weight-bold',
                'form-control mr-2'
              )}
            >
              <option value="">{l.category}</option>
              {l.tags.map((t) => (
                <option key={t} value={t}>
                  {t}
                </option>
              ))}
            </select>
          ))}
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  )
}
