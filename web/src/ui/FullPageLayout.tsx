import Head from 'next/head'
import * as React from 'react'

function FullPageLayout({
  children,
  title = ''
}: {
  title?: string
  children: React.ReactNode
}): React.ReactElement {
  return (
    <>
      <Head>
        <title>
          {title} · {process.env.NEXT_PUBLIC_SITE_DOMAIN}
        </title>
      </Head>
      <main>{children}</main>
    </>
  )
}

export default FullPageLayout
