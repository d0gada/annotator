import React from 'react'
import { Stage, Layer, Image, Text } from 'react-konva'
import useImage from 'use-image'

const DEFAULT_ASPECT_RATIO = 0.75
const START_ZOOM_LEVEL = 1
const END_ZOOM_LEVEL = 7
const ZOOM_STEP = 2
const NO_ZOOM_LEVEL = 1
const INITIAL_POS = { x: 0, y: 0 }

type Props = {
  src: string
}

type Position = {
  x: number
  y: number
}

const VideoFrame = ({ src }: Props): React.ReactElement => {
  const [image, status] = useImage(src, 'anonymous')
  const [{ x, y }, setPosition] = React.useState<Position>(INITIAL_POS)
  const [zoomLevel, setZoomLevel] = React.useState(NO_ZOOM_LEVEL)

  const enableZoom = () => {
    setZoomLevel(START_ZOOM_LEVEL)
    setPosition(INITIAL_POS)
  }

  const disableZoom = () => {
    setZoomLevel(NO_ZOOM_LEVEL)
    setPosition(INITIAL_POS)
  }

  const changeZoom = () => {
    setZoomLevel((zoom) =>
      zoom >= END_ZOOM_LEVEL ? NO_ZOOM_LEVEL : zoom + ZOOM_STEP
    )
  }
  const changePosition = (e) => {
    const pos = e.currentTarget?.getPointerPosition?.()
    if (pos) setPosition(pos)
  }

  const width = window.innerWidth - 3 * 15
  const ratio = getAspectRatio(image, status)
  const height = width * ratio
  return (
    <Stage
      width={width}
      height={height}
      onMouseMove={changePosition}
      onMouseEnter={enableZoom}
      onMouseLeave={disableZoom}
      onClick={changeZoom}
      onTap={changeZoom}
      onTouchMove={changePosition}
    >
      {status === 'loaded' && (
        <Layer
          x={-x * (zoomLevel - 1)}
          y={-y * (zoomLevel - 1)}
          scaleX={zoomLevel}
          scaleY={zoomLevel}
        >
          <Image image={image} width={width} height={height} />
        </Layer>
      )}
      {status === 'loading' && (
        <TextIndicator width={width} height={height} text="Loading..." />
      )}
      {status === 'failed' && (
        <TextIndicator
          width={width}
          height={height}
          text="Image loading failed!"
        />
      )}
    </Stage>
  )
}

const TextIndicator = ({
  width,
  height,
  fontSize = 30,
  text
}: {
  width?: number
  height?: number
  fontSize?: number
  text: string
}) => (
  <Layer>
    <Text
      fontSize={fontSize}
      height={height}
      width={width}
      align="center"
      verticalAlign="middle"
      text={text}
    />
  </Layer>
)

function getAspectRatio(image: HTMLImageElement | undefined, status: string) {
  return image && status === 'loaded'
    ? image.height / image.width
    : DEFAULT_ASPECT_RATIO
}

export default VideoFrame
