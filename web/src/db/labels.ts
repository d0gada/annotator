import type { LabelCategory } from '~/types'

const LABELS: LabelCategory[] = [
  {
    category: 'Weather Condition',
    required: true,
    tags: 'rainy,sunny,cloudy,foggy,snow,unclear'.split(',')
  },
  {
    category: 'Lane Count',
    required: true,
    tags: 'One,Two,Three,Four,Five,Six,Seven,Eight,unclear'.split(',')
  },
  {
    category: 'Environment',
    tags: 'Tunnel,highway,inner city,outer city,unclear'.split(',')
  },
  { category: 'Corrupt Image', tags: 'True,False'.split(',') }
]

export async function getLabels(): Promise<LabelCategory[]> {
  // TODO: load from db
  return LABELS
}
